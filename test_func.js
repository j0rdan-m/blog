const { openBrowser, goto, write, click } = require('taiko');
(async () => {
    await openBrowser();
    await goto("https://j0rdan-m.gitlab.io/blog/test/");
    waitFor(3000);
    await highlight('Authors');
    await click('Authors');
    await goto("https://j0rdan-m.gitlab.io/blog/test/authors/j0rdan-m/");
    waitFor(3000);
    await highlight('Follow j0rdan-m on Twitter');
    closeBrowser();
})();