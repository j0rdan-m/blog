---
title: "Gérer plusieurs auteurs grâce à la taxonomie"
date: 2020-05-18T15:49:56+02:00
authors:
- j0rdan-m
categories:
- hugo
tags:
- hugo-tips
- français
keywords:
- tech
draft: false
#thumbnailImage: //example.com/image.jpg
---

Tous les thèmes ne disposent pas de la gestion de plusieurs auteurs au sein d'un blog. Et je suis probablement mauvais observateur, car je n'ai pas vu, dans la présentation des thèmes disponibles, si un thème gère en natif plusieurs auteurs ou non.

C'est assez désagréable d'avoir trouvé LE thème parfait, et qu'une simple fonctionnalité lui manque. J'ai donc décidé de "ruser", et d'utiliser la gestion native des taxonomies, afin d'inscrire plusieurs auteurs sur un blog. Et pour le coup, ça fonctionne plutôt pas mal.

<!--more-->

Pour rédiger cet article, je me suis inspiré du tutoriel suivant : [How to create author pages](https://www.netlify.com/blog/2018/07/24/hugo-tips-how-to-create-author-pages/).

Je prends soin de le compléter uniquement sur les points pour lesquels j'ai du expérimenter un tas de choses.

# Usages des taxonomies

Il y a deux taxonomies en standard dans Hugo :

- Les tags ;
- Les catégories.

Pour utiliser les taxonomies standard d'Hugo, ou celles natives du thème choisi, c'est très simple, il suffit d'ajouter à votre archetype les éléments suivants :

```
---
tags:
- faire son site
- hugo-tips
categories:
- hugo
---
```

Le principe est donc simple : dès lors que vous ajoutez une catégorie ou un tag, et que le thème le gère bien, vous retrouvez une page ```http://[votresite]/tags/``` et une autre ```/catégories/``` qui listent les tags et catégories, et une page par item listant les articles qu mentionnent l'item dans leurs archétypes.

Voyons maintenant comment créer une nouvelle taxonomy qui ouvrirait la possibilité d'ajouter un ou plusieurs auteurs à un article.

# Créer une taxonomie

A chaque taxonomy correspond deux templates :

- list.html, qui correspond au template de la page qui décrira un des items (un auteur, pour nous, et tous les articles qui lui sont liés)
- terms.html, qui correspond à la page qui va lister tous les éléments d'une taxonomy (tous les auteurs, donc)

Dans notre cas, nous allons créer une taxonomie d'auteur. Il s'agira d'auteur de post, et non d'auteur de livres, comme dans l'exemple du lien précédent.

Nous allons travailler dans deux arborescences :

- le dossier *content*, logiquement à la racine du site hugo ;
- le dossier *layout* du thème utilisé par le site.

## Les données concernant les items de la taxonomie

Dans le dossier *content*, nous allons créer un dossier par auteur, dans lequel sera positionné un fichier *_index.md*,

```
content
└── authors/
    ├── j0rdan-m/
    │   └── _index.md
    └── author-2/
        └── _index.md
```

Chaque *_index.md* décrit un auteur, et se compose comme suit :

```
---
name: j0rdan-m
photo: 'https://j0rdan-m.gitlab.io/blog/pearlbonnet.png'
twitter: '@j0rdan_m'
---
j0rdan is an author of the blog
```

## Les méta-données

Les datas concernant les auteurs sont prêtes, il faut maintenant paramétrer les méta-datas.

Pour cela, il va falloir créer un dossier *authors* dans le layout de notre thème (et pas le dossier des taxonomies, ce qui peut surprendre).

```
my_theme/
└── layout/
    └── authors/
        └── list.html
        └── terms.html
```

Le fichier *list.html* ne liste pas les auteurs, mais les articles qui mentionnent les auteurs (ou tout autre choix de taxonomie).

Ainsi, voici le contenu basique du fichier *list.hml* :

```
<h1>{{ .Params.name }}</h1>
<img src="{{ .Params.photo }}" alt=""/>

<h2>Bio</h2>
{{ .Content }}
{{ with .Params.twitter }}
  <p>
    <a href="https://twitter.com/{{ substr . 1 }}">
      Follow {{ $.Params.name }} on Twitter
    </a>
  </p>
{{ end }}

<h2>Articles</h2>
<ul>
{{ range .Data.Pages }}
    <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
{{ end }}
</ul>

```

Le fichier *terms.html* est le template de la page qui va recenser tous les auteurs, et proposer un lien pour chacun :

```
<h1>Authors</h1>
<ul>
{{ range .Data.Pages }}
  <li><a href="{{ .Permalink }}">{{ .Params.name }}</a></li>
{{ end }}
</ul>

```

Attention, il faudra intégrer en amont et aval des codes proposés le contenu du thème, en prenant example sur la page par défaut (probablement dans *themes/my_theme/layouts/_default/single.html*)

Le thème utilisé par ce blog proposait un beau commentaire pour isoler les infos de thème du contenu (grâce à la balise ```<section></section>```. Un autre thème que j'utilise mentionne, lui <main></main>)

Puisque nous sommes sur cette page *single.html*, nous allons pouvoir y ajouter une mention de l'auteur :

```
{{- range .Params.authors }}
  {{- with $.Site.GetPage "taxonomyTerm" (printf "authors/%s" (urlize .)) }}
    <figure>
      <img src="{{ .Params.photo }}" alt=""/>
      <figcaption>
        <a href="{{ .Permalink }}">{{ .Params.name }}</a>
      </figcaption>
    </figure>
  {{ end }}
{{ end }}
```

Cette mention est à ajouter à l'endroit de la page où vous souhaitez la faire apparaitre, évidemment.

Enfin, maintenant que tout est prêt, nous allons pouvoir déclarer la taxonomie dans notre fichier config.toml, en y ajoutant la pention suivante :

```
[taxonomies]
  author = "authors"
  tag = "tags"
  category = "categories"
```

A titre personnel, j'ai ajouté manuellement une entrée dans le menu, sachant que l'adresse de visualisation de votre taxonomie sera : ```http://[mon site]/authors/```

Voilà donc comment, grâce aux taxonomies, ce blog est devenu un blog multi-auteurs.
