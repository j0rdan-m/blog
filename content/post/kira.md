---
title: "Kira"
date: 2020-04-11T21:38:57Z
draft: false
tags:
- animaux
- français
---

Qui est Kira ?

<!--more-->

![Kira](../../images/kira.jpg)

Kira est le plus beau de tous les chats du monde et de tous les temps, parce qu'elle est la seule à être amoureuse de son papa !
