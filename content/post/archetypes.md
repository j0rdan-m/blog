---
title: "Utiliser les archetypes"
date: 2020-04-17T12:49:31Z
subtitle:
draft: false
categories: 
- hugo
tags:
- hugo-tips
- français
---

Aujourd'hui, en français, je souhaite aborder le sujet des archétypes du moteur de génération de site Hugo.
Ce moteur très performant est d'ailleurs utilisé pour générer ces pages.

<!--more-->

Les archétypes sont les éléments qui permettent de générer les entêtes de page Hugo, lorsqu'on les créées avec la commande ```hugo new my_file.md```.

# archetypes/Default.md

Le default.md présent dans l'archétype s'applique à tous les "new hugo" que vous allez créer.
Ce default.md est personnalisable, comme vu dans l'article précédent, [Gérer du contenu dans un site Hugo](../content_management), mais il peut également être également multiplié.

# archetypes/mon_mien.md

Imaginons le cas d'un site d'ESN (au hasard ;) ), dans lequel on trouvera une page d'accueil, un certains nombre de page 'Produits', de page 'Offres' et une page 'blog'.

il suffira de créer, dans le folder *archetypes*, trois fichiers :

- produit.md
- offre.md
- post.md

ces trois fichiers contiendront les archétypes adequat en fonction du type de page.

Il est très simple d'appliquer un archétype à une page : l'archétype doit posséder le même nom que le répertoire hébergeant le type de page !

par exemple, pour créer une page produit, un page offre et un article de blog, nous utiliserons ces trois commandes :

```
# Nouvelle page produit
hugo new produit/eaviz.md

# Nouvelle offre Marte
hugo new offre/expert_ansible.md

# Nouvel article de blog
hugo new post/gestion_archetype.md
```

# Gestion des thèmes

A l'instar du config.toml de la section *exampleSite* d'un thème, je vous conseille vivement de repartir du ou des archetypes proposés par un thème.
Ca permettra de connaitre les bons répertoires de travail pour voir apparaitre vos pages aux endroits prévus par le thème.
