---
title: "Go on Hugo for gitlab CI/CD"
date: 2020-04-11T21:03:59Z
draft: false
tags:
- test
- english
- hugo-tips
categories:
- hugo
---

To be honnest, I wasn't sure that would be the best website FW i would discover.
Actually, using md files, fast build for html producing, and the gitlab CI/CD compliance is something very exciting !

But you should start carefully, because the quickstart isn't compliant with a gitlab Pages deployment.

<!--more-->

# How to quickstart with hugo

This is how I think you should start with Hugo :

## step 1 - Theme selection

First, choose ***carefully*** the thème you want to use. It's important because the way you'll configure your website could be (very) specific to your theme.

To choose wisely, you juste have to go on [themes.gohugo.io](https://themes.gohugo.io) and look at demo.
All demos include the markdown syntax restitution, which could allow you to be sure that the theme answer your desire !

## step 2 - Website name

Second, choose wisely the website name.
Because it has to be the folder name. when you'll deploy your website on gitlab Pages, you would have the following website ```URL : http://username.gitlab.io/foldername/```

If you're creating a blog, "blog" would be a good name. If you're on website to propose a product, "productName" would be wise.

## step 3 - website creation

Easiest part of the tutorial. Do nothing else typing the following command :

```
hugo new site <foldername>
```

Now, you've a new folder called *foldername* with some stuff in it. Before doing anything, follow the next step. DO NOT ENTER IN THE NEW FOLDER

## step 4 - theme downloading

Go on your chosen theme repository and DO NOT CLONE the git repository. Get the zip file, and unzip it on *foldername/themes/themeName*.

Be carefull, the content of the theme has to be on *themeName*, not on *themeName/hugo-theme-name-master*.

## step 5 - Use exampleSite as template

Copy the *themeName/exampleSite/config.toml* directly on your *foldername*, replacing the standard one.

On the config.toml file, modify the theme selection line by

```
theme = "<themeName>"
```

Since you want to use hugo with gitlab pages, you should also modify the _baseURL_ on config.toml :

```
baseurl = "https://<username>.gitlab.io/<foldername>"
```

# Gitlab Pages deployment

## Advertising

DO NOT GIT INIT ANYTHING ! You'll create a gitlab project and you'll init

## step 6 - Create gitlab project

On gitlab, create a new projet, called by the same name as your folder name. Do not initialize it with a readme or something else.

## step 7 - Create your CI file

Go on your folder name, and create a file called ```.gitlab-ci.yml```

Edit the file with the following code :

```
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

## step 8 - Push your website

First, init the repo :

```
git init
```

Then, add /public directory to our .gitignore file

```
echo "/public" >> .gitignore
```

Finally, commit and push code to master branch

```
git add .
git commit -m "Initial commit"
git remote add origin https://gitlab.com/YourUsername/your-hugo-site.git
git push -u origin master
```

## step 9 - See & Wait

Go on the "CI/CD pipeline" tab of your gitlab repository

Check the build, and wait for passed. You're in ! Your website is published on *username*.gitlab.io/*foldername*

# Other stuff

You won't believe it !
