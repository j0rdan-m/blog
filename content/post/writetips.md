---
title: "Write a readable tuto with Markdown"
date: 2020-04-28T18:49:31Z
subtitle: Please, make it readable
draft: false
categories:
- hugo
tags:
- hugo-tips
- english
authors:
- j0rdan-m
---

Hi everyone,

Here is how I write technicals articles, to make it readable, and usefull for the future myself who want to remember how to use that techni I wrote an article about several month ago.

<!--more-->

What you're reading is the introduction. I start by intro, because my theme take that text as summary in the first page.

Here is my post.md file beginning :

```

*my archetype*
--

Aujourd'hui, en français, je souhaite aborder le sujet des archétypes du moteur de génération de site Hugo. Ce moteur très performant est d'ailleurs utilisé pour générer ces pages.

<!--more-->
```

Some theme understand the "more balise" so my intro ends with it.

I can end my intro after the ```<!--more-->``` if I think it's too long, or something else.

# Titles

I continue the post by using title. I think Title is important, because it allow us understand the structure of the article, and allow "power users" to go directly to the part they except to find informations.

on my previous article, about archetypes, my titles was :

```
# archetypes/Default.md

(...)

# archetypes/mon_mien.md

(...)
```

I could have used a more complex structure by using second or third level of titles

```
# First level

## Second level

### Third level
```

That code will be displayed like this on the blog :

- - -

## First level

blablabla

### Second level

blablabla explained

#### Third level

blablabla more specified

- - -

It's important to put Title on your tutos. Sometimes, readers would read more than one tuto at the same time, and it's easier to go to next part when title organized the page.

# Code display

There is two kinds of way to put code on markdown :

## inline code

This is when you use ```` ``` 'my code is cool' ``` ```` in the same line than the rest of the text

## single line code isolated

This is when you use :

````
```
my code is cool
```
````

The code is isolated from the rest of the paragraph

## My advise

Always use the second one. For a simple reason : People do use copy/paste. And it is so easier to copy paste a single line.

# Next tips

I will add some tips here in the future :

## Bullets

- Do not
- use
- bullet
- everywhere

Do it when it's usefull. There are two cases :

- list of stuff
- list of stuff

## Use quote for quoting

Use quote for quote. I know it sounds weird, but you could want to use the nice quote format for something else. Don't.

> Quote style is for quote - Dr. CssProvider

## incoming tips
