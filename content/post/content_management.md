---
title: "Gérer du contenu dans un site Hugo"
date: 2020-04-14T09:39:30Z
draft: false
subtitle: Comment gérer du contenu dans un site statique généré par le moteur Hugo"
tags:
- français
- hugo-tips
categories:
- hugo
---

Cet article a l'objectif très simple de démystifier les éléments qu'on souhaite avoir sur le site (image dans les articles, avatar, etc.). C'est d'autant plus subtil que le comportement qu'on obtient avec Hugo en tant que serveur web, un site Hugo déployé sour gitlab Pages ou un cluster cloud ne sont pas les mêmes en fonction du standard que l'on choisit d'appliquer.

<!--more-->

Le but de ce post est d'éclairer la manière de procéder pour faciliter la création de post.

*Disclaimer : le tutoriel officiel est ****très mauvais**** dans le cas d'un site que l'on souhaite déployer sous gitlab Pages !*

- - -

# Gestion des pages

Dans le cadre d'un site en mode blog, c'est relativement simple.

La commande ```hugo new <repertoire>/<nom d'article>.md``` permet d'ajouter des articles au format MD dans le dossier "répertoire". Usuellement, on utilisera "post" pour un blog.

- - -

## Propriété d'un page

Les propriétés d'une page se gèrent dans l'entête des pages créées.
elle se retrouvent sous la forme suivante :

```
---
title: "mon article"
date: AAAA/MM/DD/THH:MM:SS
draft: true
```

Il s'agit du standard généré par Hugo. Deux éléments sont à noter :

1. Dans un déploiement dans gitlab, seule les pages "draft: false" s'affichent. Les pages "draft: true" sont ignorées lors du build du site. Il est alors possible de builder un site en local (avec l'option -D pour draft) et tester certaines pages tout en profitant de l'hébergement gitlab.
1. La version de base peut être complétée avec d'autres informations concernant la page créée :

- subtitle: pour ajouter un sous-titre
- tags: permet d'ajouter des tags à un article
- categories: permet de catégoriser les articles

Bref, rien de foufou, mais c'est utile. Ainsi, l'entête de l'article que vous êtes en train de lire est :

```
---
title: "Gérer du contenu dans un site Hugo"
date: 2020-04-14T09:39:30Z
draft: false
subtitle: "Comment gérer du contenu dans un site statique généré par Hugo"
tags:
- français
- hugo-tips
categories:
- hugo
---
```

- - -

## Gestion des images

Pour gérer les images affichées dans un article, voici la solution que je propose, et qui a l'avantage de marcher avec hugo en local, mais également avec un déploiement gitlab :

1. Créer un sous-dossier "images" dans le répertoire "content"
1. Y déposer les images que vous souhaitez faire apparaitre dans votre article
1. ajouter ```![texte alternatif](../../images/mon_image.jpg)``` dans votre article, à l'endroit où vous souhaitez faire apparaitre l'image.

- - -

# Gérer le contenu liés aux thèmes (photos de fond, avatars, etc.)

Dans ces cas là, la gestion des images locales peut être compliquées à gérer si on souhaite que le fonctionnement soit identique que l'on utilise le moteur hugo en local ou dans la version Gitlab Pages.

C'est pour cette raison que je propose de le gérer de la manière suivante :

Créer un répertoire appelé "static" à la racine de votre site. Placez-y directement les images que vous souhaitez utiliser, sans les mettre dans un sous-répertoire. Elles seront alors utilisables dans votre fichier de paramétrage config.toml en donnant simplement leur nom.
